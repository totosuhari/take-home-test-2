<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Swag Labs_user-name</name>
   <tag></tag>
   <elementGuidId>45fbfdae-2ae0-47c5-9cb6-bd89d429608c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='user-name']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#user-name</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>99320243-2272-4579-9fbd-4659ba41c71a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input_error form_input</value>
      <webElementGuid>ec184b2f-0e57-4049-af3f-b59e2b407393</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Username</value>
      <webElementGuid>47f97ffe-bd39-41dd-8b2d-199e01f5b5a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>053f42de-5399-4e0d-8e14-fdbca6e6ca6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>7a799247-75d2-45cf-9300-f5ec3d6bbcc9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user-name</value>
      <webElementGuid>6f695801-ccf8-4243-87c8-b626f626e53c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>user-name</value>
      <webElementGuid>c4ade934-8243-460b-a56d-b0cf00c4de52</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>9f25e362-07d2-4356-8bde-53088c74a71d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>c7634daf-2aaf-4a19-9d9d-435ee7c148fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;user-name&quot;)</value>
      <webElementGuid>3dfe3559-56b3-46c8-95a0-5b459d135320</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='user-name']</value>
      <webElementGuid>bf57787e-7ff5-4fab-8749-afa6d5d23790</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='login_button_container']/div/form/div/input</value>
      <webElementGuid>bd100922-6263-48dd-8690-666b639e370e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>5e870bdc-15ff-440e-9066-c05c22260755</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Username' and @type = 'text' and @id = 'user-name' and @name = 'user-name']</value>
      <webElementGuid>d2341b1b-bbc7-4c0e-8e50-141acb8a6126</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
